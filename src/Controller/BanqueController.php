<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BanqueController extends AbstractController
{
    /**
     * @Route("/banque", name="banque_index")
     */
    public function index(): Response
    {
        return $this->render('banque/index.html.twig');
    }
}
