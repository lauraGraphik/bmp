<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AssuranceController extends AbstractController
{
    /**
     * @Route("/assurance", name="assurance_index")
     */
    public function index(): Response
    {
        return $this->render('assurance/index.html.twig', [
            'controller_name' => 'AssuranceController',
        ]);
    }
    /**
     * @Route("/assurance/professionnel", name="assurance_professionnel")
     */
    public function professionnel(): Response
    {
        return $this->render('assurance/professionnel.html.twig');
    }
    /**
     * @Route("/assurance/particulier", name="assurance_particulier")
     */
    public function particulier(): Response
    {
        return $this->render('assurance/particulier.html.twig');
    }
}
